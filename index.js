const request = require("request-promise");
module.exports = class CouplePlans {
    constructor(options) {
        if(options) {
            this.isProduction = options.isProduction;
            this.urlSuffix = options.sourceField ? `?sourceField=${options.sourceField}`: ``;
            this.username = options.username;
            this.password = options.password;
        }
    }

    /** PRODUCTS (services) */
    addProduct(payload) {
        return this._product("POST", "", payload);
    }
    getProduct(resourceIdentifier) {
        return this._product("GET", resourceIdentifier);
    }
    getProducts() {
        return this._product("GET", "");
    }
    updateProduct(resourceIdentifier, payload) {
        return this._product("PUT", resourceIdentifier, payload);
    }
    deleteProduct(resourceIdentifier, payload) {
        return this._product("DELETE", resourceIdentifier, payload);
    }

    /** PLANS */
    addPlan(resourceIdentifier, payload) {
        return this._plan("POST", resourceIdentifier, payload);
    }
    updatePlan(resourceIdentifier, payload) {
        return this._plan("PUT", resourceIdentifier, payload);
    }
    deletePlan(resourceIdentifier, payload) {
        return this._plan("DELETE", resourceIdentifier, payload);
    }

    /** USERS */
    addUser(payload) {
        return this._user("POST", "", payload);
    }
    getUser(resourceIdentifier) {
        return this._user("GET", resourceIdentifier);
    }
    getUsers() {
        return this._user("GET", "");
    }
    updateUser(resourceIdentifier, payload) {
        return this._user("PUT", resourceIdentifier, payload);
    }
    deleteUser(resourceIdentifier) {
        return this._user("DELETE", resourceIdentifier);
    }

    /** ADDRESS */
    addAddress(resourceIdentifier, payload) {
        return this._address("POST", resourceIdentifier, payload);
    }
    updateAddress(resourceIdentifier, payload) {
        return this._address("PUT", resourceIdentifier, payload);
    }
    deleteAddress(resourceIdentifier, payload) {
        return this._address("DELETE", resourceIdentifier, payload);
    }

    /** SEQUENCES */
    addSequence(payload) {
        return this._sequence("POST", "", payload);
    }
    getSequence(resourceIdentifier) {
        return this._sequence("GET", resourceIdentifier);
    }
    getSequences() {
        return this._sequence("GET", "");
    }
    updateSequence(resourceIdentifier, payload) {
        return this._sequence("PUT", resourceIdentifier, payload);
    }
    deleteSequence(resourceIdentifier) {
        return this._sequence("DELETE", resourceIdentifier);
    }

    /** STEPS */
    addStep(resourceIdentifier, payload) {
        return this._step("POST", resourceIdentifier, payload);
    }
    updateStep(resourceIdentifier, payload) {
        return this._step("PUT", resourceIdentifier, payload);
    }
    deleteStep(resourceIdentifier, payload) {
        return this._step("DELETE", resourceIdentifier, payload);
    }

    /** Resource Types */
    _user(method, resourceIdentifier, payload) {
        const url = `${this._getBaseUrl()}/user/${resourceIdentifier}${this.urlSuffix}`;
        return this._makeRequest(url, method, payload);
    }
    _address(method, resourceIdentifier, payload) {
        const url = `${this._getBaseUrl()}/user/${resourceIdentifier}/address${this.urlSuffix}`;
        return this._makeRequest(url, method, payload);
    }
    _sequence(method, resourceIdentifier, payload) {
        const url = `${this._getBaseUrl()}/sequence/${resourceIdentifier}${this.urlSuffix}`;
        return this._makeRequest(url, method, payload);
    }
    _step(method, resourceIdentifier, payload) {
        const url = `${this._getBaseUrl()}/sequence/${resourceIdentifier}/step${this.urlSuffix}`;
        return this._makeRequest(url, method, payload);
    }
    _product(method, resourceIdentifier, payload) {
        const url = `${this._getBaseUrl()}/product/${resourceIdentifier}${this.urlSuffix}`;
        return this._makeRequest(url, method, payload);
    }
    _plan(method, resourceIdentifier, payload) {
        const url = `${this._getBaseUrl()}/product/${resourceIdentifier}/plan${this.urlSuffix}`;
        return this._makeRequest(url, method, payload);
    }
    /** Helper Functions */
    _getBaseUrl() {
        return `https://${this.isProduction ? "api" : "staging-api"}.coupleplans.com`;
    }
    _getAuthenticationHeader() {
        const authenticationHeader = new Buffer(this.username + ':' + this.password).toString('base64');
        return {
            'Authorization': 'Basic ' + authenticationHeader
        };
    }
    _makeRequest(url, method, payload = {}) {
        const options = {
            method,
            url,
            headers: this._getAuthenticationHeader(),
            form: payload
        };
        return request(url, options);
    }
}